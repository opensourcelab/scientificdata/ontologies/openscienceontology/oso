"""_____________________________________________________________________

:PROJECT: Open Science Ontology

* Main module implementation *

:details:  Main module implementation.

.. note:: -
.. todo:: - 
________________________________________________________________________
"""


import logging

from .oso_emmo_interface import OSOInterface
from .__init__ import __version__ # Version of this ontology

from oso_measurements.measurements import OSO_Measurement

class OSO_Emmo(OSOInterface):
    def __init__(self) -> None:
        """Implementation of the OSOInterface
        """
        oso_meas = OSO_Measurement()

        
        
    def save_ontology(self, filename : str = None,  format='turtle'):
        """ """
