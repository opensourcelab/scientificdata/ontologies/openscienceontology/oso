#!/usr/bin/env python
"""Tests for `oso_emmo` package."""
# pylint: disable=redefined-outer-name
from oso_emmo import __version__
from oso_emmo.oso_emmo_interface import OSOInterface
from oso_emmo.oso_emmo_impl import OSO_Emmo

def test_version():
    """Sample pytest test function."""
    assert __version__ == "0.0.1"

def test_OSOInterface():
    """ testing the formal interface (GreeterInterface)
    """
    assert issubclass(OSO_Emmo, OSOInterface)

def test_save_ontology():
    """ Testing saving
    """
    oso_onto = OSO_Emmo()
    filename = 'oso_test_ontology.owl'
    format = 'turtle'
    #oso_onto.save_ontology(filename=filename, format=format)
    #assert hw.greet_the_world(name) == f"Hello world, {name} !"

